# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

# Author:
---------
- Austin Mello

# Contact Information:
----------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

# Description:
--------------
- A simple algorithm to calculate control opening/closing times for brevets of
  different distances.
- This algorithm does its best to follow RUSA regulations to the letter.  If 
  certain cases were not clarified, certain assumptions were implemented.  
  Assumptions are described in further detail below.

# Links to official RUSA regulations:
-------------------------------------
- Official Algorithm: https://rusa.org/pages/acp-brevet-control-times-calculator
- Sample calculator: https://rusa.org/octime_acp.html
- Official RUSA rules/regulations: https://rusa.org/pages/rulesForRiders

# Assumptions:
--------------
- 1) ALL NON-NUMERICAL INPUT WILL RESULT IN A 404.

- 2) Overlap of certain distances in the table will refer to the speed in the
     previous range.
     - Example: A control at 600km will have a minimum speed of 15km/hr and a
       max of 30km/hr.

- 3) Any control that is located 20% further than the brevet distance will 
     result in a redirect to an error page.
     - Example: One cannot place a control further than 240km in a 200km brevet

- 4) Closing times at the final control point will have a set time accordance
     with Article 9 of the RUSA regulations linked above.  

- 5) The sample calculator linked above has set control points at or beyond the 
     total brevet distance at a set time.  These fixtures are not stated 
     explicitly in the RUSA regulations, so I left them out.  Instead, the final
     control point will open IN ACCORDANCE WITH THE TOTAL BREVIT DISTANCE.
     - Example: In a 200km brevet, a control point at 205km will open at 6 
       hours, 2 minutes, while a control at 200km would open at 5 hours, 53
       minutes.

- 6) Controls under 60km follow the RUSA algorithm outlined in the "oddities"
     section of the official algorithm page linked above.

# Notes:
--------
