import arrow
import time
import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
        level = logging.WARNING)
log = logging.getLogger(__name__)

from acp_times import open_time

"""OPENING TIMES TESTS"""

def test_start_empty():
    #tests for empty set in start time
    timestamp = ""
    opening = open_time(42, 200, timestamp)
    assert opening == "Bad input"

def test_control_empty():
    #tests for empty set in the control field
    timestamp = arrow.now()
    control = ""
    opening = open_time(control, 200, timestamp)
    assert opening == "Bad input"

def test_negative():
    #tests for negative inputs
    control = -1
    timestamp = arrow.now()
    opening = open_time(control, 200, timestamp)
    assert opening == "Bad input"

def test_max_dist():
    #tests for control > 20% of total brevet distance
    control = 250
    timestamp = arrow.now()
    opening = open_time(control, 200, timestamp)
    assert opening == "Bad input"

def test_twohundred():
    control = 200.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 200, timestamp)
    shifted = timestamp.shift(hours = 5, minutes = 53)
    assert opening == shifted, opening

def test_open_oddity():
    control = 205.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 200, timestamp)
    shifted = timestamp.shift(hours = 6, minutes = 2)
    assert opening == shifted, opening

def test_threehundred():
    control = 300.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 300, timestamp)
    shifted = timestamp.shift(hours = 9)
    print(shifted)
    assert opening == shifted, opening

def test_fourhundred():
    control = 400.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 400, timestamp)
    shifted = timestamp.shift(hours = 12, minutes = 8)
    print(shifted)
    assert opening == shifted, opening

def test_sixhundred():
    control = 600.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 600, timestamp)
    shifted = timestamp.shift(hours = 18, minutes = 48)
    print(shifted)
    assert opening == shifted, opening

def test_thousand():
    control = 1000.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 1000, timestamp)
    shifted = timestamp.shift(hours = 33, minutes = 5)
    print(shifted)
    assert opening == shifted, opening

def test_funkyone():
    control = 500.0
    timestamp = arrow.get(2020, 5, 14)
    opening = open_time(control, 600, timestamp)
    shifted = timestamp.shift(hours = 15, minutes = 28)
    print(shifted)
    assert opening == shifted, opening
