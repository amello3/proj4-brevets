"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times", methods = ['GET', 'POST'])
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.info("Got a JSON request")
    km = request.args.get('km', -1, type=float)
    length = request.args.get('length', 0, type = int)
    date = request.args.get('date', type = str)
    time = request.args.get('time', type = str)
    datetime = date + " " + time
    datetime = arrow.get(datetime)
    
    app.logger.info("km={}".format(km))
    app.logger.info("brevet length={}".format(length))
    app.logger.info("starting time={}".format(datetime))
    app.logger.info("date={}".format(date))
    app.logger.info("time={}".format(time))
    app.logger.info("request.args: {}".format(request.args))
    
    open_time = acp_times.open_time(km, length, datetime)
    close_time = acp_times.close_time(km, length, datetime)
    
    app.logger.info("Open time={}" .format(open_time))
    app.logger.info("Close time={}" .format(close_time))
    
    if (open_time == "Bad input" or close_time == "Bad input"):
        return flask.jsonify(result = "404")

    else:
        open_time = arrow.get(open_time).for_json()
        close_time = arrow.get(close_time).for_json()
        result = {"open": open_time, "close": close_time}
        app.logger.warning(result)
    
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
