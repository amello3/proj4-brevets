import arrow
import time
import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
        level = logging.WARNING)
log = logging.getLogger(__name__)

from acp_times import close_time

"""CLOSING TIMES TESTS"""

def test_start_empty():
    #tests for empty set in start time
    timestamp = ""
    closing = close_time(42, 200, timestamp)
    assert closing == "Bad input"

def test_control_empty():
    #tests for empty set in the control field
    timestamp = arrow.now()
    control = ""
    closing = close_time(control, 200, timestamp)
    assert closing == "Bad input"

def test_negative():
    #tests for negative inputs
    control = -1
    timestamp = arrow.now()
    closing = close_time(control, 200, timestamp)
    assert closing == "Bad input"

def test_max_dist():
    #tests for control > 20% of total brevet distance
    control = 250
    timestamp = arrow.now()
    closing = close_time(control, 200, timestamp)
    assert closing == "Bad input"

def test_twohundo():
    control = 200.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 200, timestamp)
    shifted = timestamp.shift(hours = 13, minutes = 30)
    assert closing == shifted, closing    

def test_close_oddity():
    control = 205.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 200, timestamp)
    shifted = timestamp.shift(hours = 13, minutes = 30)
    assert closing == shifted, closing

def test_threehundo():
    control = 300.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 300, timestamp)
    shifted = timestamp.shift(hours = 20)
    print(shifted)
    assert closing == shifted, closing

def test_fourhundo():
    control = 400.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 400, timestamp)
    shifted = timestamp.shift(hours = 27)
    print(shifted)
    assert closing == shifted, closing

def test_sixhundo():
    control = 600.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 600, timestamp)
    shifted = timestamp.shift(hours = 40)
    print(shifted)
    assert closing == shifted, closing

def test_thouso():
    control = 1000.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 1000, timestamp)
    shifted = timestamp.shift(hours = 75)
    print(shifted)
    assert closing == shifted, closing

def test_funkytwo():
    control = 500.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 600, timestamp)
    shifted = timestamp.shift(hours = 33, minutes = 20)
    print(shifted)
    assert closing == shifted, closing

def test_funkythree():
    control = 800.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 1000, timestamp)
    shifted = timestamp.shift(hours = 57, minutes = 30)
    print(shifted)
    assert closing == shifted, closing

def test_sub_sixty():
    control = 40.0
    timestamp = arrow.get(2020, 5, 14)
    closing = close_time(control, 200, timestamp)
    shifted = timestamp.shift(hours = 3)
    print(shifted)
    assert closing == shifted, closing

